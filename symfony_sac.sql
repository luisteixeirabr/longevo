--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.6
-- Dumped by pg_dump version 9.5.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Name: client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('client_id_seq', 4, true);


--
-- Data for Name: customers; Type: TABLE DATA; Schema: public; Owner: root
--

COPY customers (id, name, email) FROM stdin;
1	Luis	luis@bbb.com.br
2	José	jose@bbb.com.br
3	Maria	maria@bbb.com.br
4	Carla	carla@bbb.com.br
6	Vivi	vivi@bbb.com.br
5	Renata	renata@bbb.com.br
\.


--
-- Name: customers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('customers_id_seq', 6, true);


--
-- Name: order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('order_id_seq', 1, false);


--
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: root
--

COPY orders (id, customer_id, product_name) FROM stdin;
1	1	Teclado
2	1	Monitor
3	2	Mac
4	3	Mouse comum
5	4	HD
\.


--
-- Name: orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('orders_id_seq', 10, true);


--
-- Data for Name: sac; Type: TABLE DATA; Schema: public; Owner: root
--

COPY sac (id, customer_name, title, obs, customer_id, order_id) FROM stdin;
\.


--
-- Name: sac_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('sac_id_seq', 12, true);


--
-- PostgreSQL database dump complete
--

