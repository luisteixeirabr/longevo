<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * AppBundle\Order
 *
 * @ORM\Entity()
 * @ORM\Table(name="orders")
 */
class Order
{
    /**
     * @var integer $id
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Many Order have One Customer.
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="orders")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    private $customer;

    /**
     * One Order has Many Sac.
     * @ORM\OneToMany(targetEntity="Sac", mappedBy="order")
     */
    private $sacs;

    /**
     * @var integer $productName
     *
     * @ORM\Column(name="product_name", type="string", length=100)
     * @Assert\NotBlank
     */
    private $productName;

    public function __construct() {
        $this->sacs = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->id;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productName
     *
     * @param string $productName
     *
     * @return Order
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;

        return $this;
    }

    /**
     * Get productName
     *
     * @return string
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * Set customer
     *
     * @param \AppBundle\Entity\Customer $customer
     *
     * @return Order
     */
    public function setCustomer(\AppBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \AppBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Add sac
     *
     * @param \AppBundle\Entity\Sac $sac
     *
     * @return Order
     */
    public function addSac(\AppBundle\Entity\Sac $sac)
    {
        $this->sacs[] = $sac;

        return $this;
    }

    /**
     * Remove sac
     *
     * @param \AppBundle\Entity\Sac $sac
     */
    public function removeSac(\AppBundle\Entity\Sac $sac)
    {
        $this->sacs->removeElement($sac);
    }

    /**
     * Get sacs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSacs()
    {
        return $this->sacs;
    }
}
