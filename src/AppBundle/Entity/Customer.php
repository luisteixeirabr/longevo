<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * AppBundle\Customer
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CustomerRepository")
 * @ORM\Table(name="customers")
 */
class Customer
{
    /**
     * @var integer $id
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=100)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * One customer has Many Sac.
     * @ORM\OneToMany(targetEntity="Sac", mappedBy="customer")
     */
    private $sacs;

    /**
     * One customer has Many Orders.
     * @ORM\OneToMany(targetEntity="Order", mappedBy="customer")
     */
    private $orders;

    public function __construct() {
        $this->order = new ArrayCollection();
        $this->sacs = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=100)
     */
    private $email;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Customer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Customer
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Add sac
     *
     * @param \AppBundle\Entity\Sac $sac
     *
     * @return Customer
     */
    public function addSac(\AppBundle\Entity\Sac $sac)
    {
        $this->sac[] = $sac;

        return $this;
    }

    /**
     * Remove sac
     *
     * @param \AppBundle\Entity\Sac $sac
     */
    public function removeSac(\AppBundle\Entity\Sac $sac)
    {
        $this->sac->removeElement($sac);
    }

    /**
     * Get sac
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSac()
    {
        return $this->sac;
    }

    /**
     * Add order
     *
     * @param \AppBundle\Entity\Order $order
     *
     * @return Customer
     */
    public function addOrder(\AppBundle\Entity\Order $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param \AppBundle\Entity\Order $order
     */
    public function removeOrder(\AppBundle\Entity\Order $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Get sacs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSacs()
    {
        return $this->sacs;
    }
}
