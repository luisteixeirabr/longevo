<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Sac;
use AppBundle\Entity\Customer;
use AppBundle\Entity\Order;
use AppBundle\Form\Type\SacType;

class SacController extends Controller
{
    /**
     * Matches / exactly
     *
     * @Route("/", name="sac_index")
     */
    public function rootAction()
    {
        $count = [];
        $repository = $this->getDoctrine()->getRepository('AppBundle:Sac');
        $count["sac"] = count($repository->findAll());

        $repository = $this->getDoctrine()->getRepository('AppBundle:Customer');
        $count["customer"] = count($repository->findAll());

        $repository = $this->getDoctrine()->getRepository('AppBundle:Order');
        $count["order"] = count($repository->findAll());

        return $this->render('sac/index.html.twig', array(
            'report' => $count
        ));
    }

    /**
     * Matches /sac exactly
     *
     * @Route("/sac", name="sac_list")
     */
    public function indexAction(Request $request)
    {
        $search = $request->query->get("search");

        // search
        if($search['term']){
            if($search['option']=='email') {
                $repository = $this->getDoctrine()->getRepository('AppBundle:Customer');
                $customer = $repository->findOneByEmail($search['term']);

                if(!$customer){
                    return $this->redirectToRoute('sac_list');
                }

                $repository = $this->getDoctrine()
                    ->getRepository('AppBundle:Sac');

                $query = $repository->createQueryBuilder('s')
                    ->where('s.customer = :pcustomer')
                    ->setParameter('pcustomer', $customer->getId())
                    ->getQuery();
            }

            if($search['option']=='order') {
                $repository = $this->getDoctrine()->getRepository('AppBundle:Order');
                $order = $repository->find($search['term']);

                if(!$order){
                    return $this->redirectToRoute('sac_list');
                }

                $repository = $this->getDoctrine()
                    ->getRepository('AppBundle:Sac');

                $query = $repository->createQueryBuilder('s')
                    ->where('s.order = :porder')
                    ->setParameter('porder', $order->getId())
                    ->getQuery();
            }

            if($query){
                $paginator  = $this->get('knp_paginator');
                $pagination = $paginator->paginate(
                $query, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                5/*limit per page*/);

                return $this->render('sac/list.html.twig', array('pagination' => $pagination));
            }
        }

        $em = $this->getDoctrine()->getManager();
        $dql   = "SELECT a FROM AppBundle:Sac a order by a.id desc";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
        $query, /* query NOT result */
        $request->query->getInt('page', 1)/*page number*/,
        5/*limit per page*/);

        // parameters to template
        return $this->render('sac/list.html.twig', array('pagination' => $pagination));
    }

    /**
    * Matches /sac/search
    *
    * @Route("/sac/search", name="sac_search")
    */
    public function searchAction(Request $request)
    {
        $search = [];
        $search['term'] = $request->query->get('term');
        $search['option'] = $request->query->get('option');

        if(count($search)>1){

            return $this->redirectToRoute('sac_list', array('search' => $search));
        }
    }

    /**
     * Matches /sac/new
     *
     * @Route("/sac/new", name="sac_new")
     */
    public function newAction(Request $request)
    {
        // just setup a fresh $sac object
        $sac = new Sac();

        $form = $this->createForm(SacType::class, $sac);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $formSac = $form->getData();

            $email = $form->get('customerEmail')->getData();
            $orderId = (int) $form->get('orderId')->getData();

            $em = $this->getDoctrine()->getManager();

            //Check order exist
            $order = $this->getDoctrine()
                ->getRepository('AppBundle:Order')
                ->findOneById($orderId);

            if(!$order){
                $this->addFlash(
                    'danger',
                    'Não existe número de pedido (' .$orderId. ').'
                );

                return $this->render('sac/new.html.twig', array(
                    'form' => $form->createView(),
                ));
            }

            //Check customer exist or create
            $customer = $this->getDoctrine()
                ->getRepository('AppBundle:Customer')
                ->findOneByEmail($email);

            if(!$customer) {
                $customer = new Customer();
                $customer->setName($formSac->getName());
                $customer->setEmail($email);

                $em->persist($customer);
                $em->flush();

                $this->addFlash(
                    'info',
                    'Criado novo usuário ' . $customer->getName() . '.'
                );
            }

            $customerObj = $this->getDoctrine()
                ->getRepository("AppBundle:Customer")
                ->findOneBy(
                    ["id" => $customer->getId()]
            );

            $sac->setCustomer($customerObj);

            $orderObj = $this->getDoctrine()
                ->getRepository("AppBundle:Order")
                ->findOneBy(
                    ["id" => $order->getId()]
            );
            $sac->setOrder($orderObj);

            $em->persist($sac);
            $em->flush();

            return $this->redirectToRoute('sac_success');
        }

        $validator = $this->get('validator');
        $errors = $validator->validate($sac);

        $errorsString = null;

        if (count($errors) > 0) {
            /*
             * Uses a __toString method on the $errors variable which is a
             * ConstraintViolationList object. This gives us a nice string
             * for debugging.
             */
            $errorsString = (string) $errors;

           // return new Response($errorsString);
        }


        return $this->render('sac/new.html.twig', array(
            'form' => $form->createView(),
           // 'errors' => $errorsString,
        ));
    }


    /**
     * Matches /sac/success
     *
     * @Route("/sac/success", name="sac_success")
     */
    public function successAction()
    {
        $this->addFlash(
            'success',
            'Salvo!'
        );

        return $this->redirectToRoute('sac_list');
    }
}
