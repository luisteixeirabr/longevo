<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Order;

class OrderController extends Controller
{
    /**
     * Matches /order exactly
     *
     * @Route("/order", name="order_list")
     */
    public function indexAction()
    {
        $orders = $this->getDoctrine()
            ->getRepository('AppBundle:Order')
            ->findAll();

        return $this->render('order/list.html.twig', array(
            'orders' => $orders
        ));
    }

    /**
     * Matches /order/new
     *
     * @Route("/order/new", name="order_new")
     */
    public function newAction(Request $request)
    {
        $productName = $request->query->get("product_name");
        $customerId = $request->query->get("customer_id");

        if(!$productName || !$customerId) {
            return new Response('Informe nome do produto e id do cliente na querystring ');
        }

        //get customer object
        $repository = $this->getDoctrine()->getRepository('AppBundle:Customer');

        $customer = $repository->find($customerId);

        if(!$customer){
            return new Response('Id do cliente não existe');
        }

        $order = new Order();
        $order->setCustomer($customer);
        $order->setProductName($productName);

        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();

        return new Response('Saved new Order with id '.$order->getId());
    }
}
