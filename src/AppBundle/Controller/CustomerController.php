<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Customer;

class CustomerController extends Controller
{
    /**
     * Matches /sac exactly
     *
     * @Route("/customer", name="customer_list")
     */
    public function indexAction()
    {
        $customers = $this->getDoctrine()
            ->getRepository('AppBundle:Customer')
            ->findAll();

        return $this->render('customer/list.html.twig', array(
            'customers' => $customers
        ));
    }

    /**
     * Matches /customer/new
     *
     * @Route("/customer/new", name="customer_new")
     */
    public function newAction(Request $request)
    {
        $name = $request->query->get("name");
        $email = $request->query->get("email");

        if(!$name && !$email) {
            return new Response('Informe nome e email na querystring ');
        }

        $customer = new Customer();
        $customer->setName($name);
        $customer->setEmail($email);

        $em = $this->getDoctrine()->getManager();
        $em->persist($customer);
        $em->flush();

        return new Response('Saved new Customer with id '.$customer->getId());
    }
}
