<?php
// AppBundle/Form/SacType.php
namespace AppBundle\Form\Type;

use AppBundle\Entity\Sac;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use AppBundle\Form\Type\CustomerType;
use AppBundle\Form\Type\OrderType;


class SacType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('label' => 'Nome do cliente'))
            ->add('orderId', TextType::class, array('label' => 'Número do pedido', 'mapped'  => false))
            ->add('customerEmail', EmailType::class, array('label' => 'Email', 'mapped'  => false))
            ->add('title', TextType::class, array('label' => 'Título'))
            ->add('obs', TextareaType::class, array('label' => 'Obs'))
            ->add('save', SubmitType::class, array('label' => 'Enviar'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Sac::class,
        ));
    }
}
