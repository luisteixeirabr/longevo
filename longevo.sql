--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.6
-- Dumped by pg_dump version 9.5.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: customers; Type: TABLE DATA; Schema: public; Owner: root
--

COPY customers (id, name, email) FROM stdin;
1	Luis	luis@bbb.com.br
2	José	jose@bbb.com.br
3	Maria	maria@bbb.com.br
4	Carla	carla@bbb.com.br
6	Vivi	vivi@bbb.com.br
5	Renata	renata@bbb.com.br
\.


--
-- Name: customers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('customers_id_seq', 6, true);


--
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: root
--

COPY orders (id, customer_id, product_name) FROM stdin;
1	1	Teclado
2	1	Monitor
3	2	Mac
4	3	Mouse comum
5	4	HD
6	6	Notebook
\.


--
-- Name: orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('orders_id_seq', 10, true);


--
-- Data for Name: sac; Type: TABLE DATA; Schema: public; Owner: root
--

COPY sac (id, customer_id, order_id, customer_name, title, obs) FROM stdin;
1	1	1	Luis	Chamado 1	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc feugiat egestas odio, in gravida diam.
2	3	4	Maria	Chamado 2	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc feugiat egestas odio, in gravida diam.
3	4	5	Carla	Chamado 3	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc feugiat egestas odio, in gravida diam.
4	6	6	Vivi	Chamado 4	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc feugiat egestas odio, in gravida diam.
5	2	3	José	Chamado 5	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc feugiat egestas odio, in gravida diam.
6	1	2	Luis	Chamado 6	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc feugiat egestas odio, in gravida diam.
\.


--
-- Name: sac_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('sac_id_seq', 6, true);


--
-- PostgreSQL database dump complete
--

