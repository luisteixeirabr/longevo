--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.6
-- Dumped by pg_dump version 9.5.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: customers; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE customers (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    email character varying(100) NOT NULL
);


ALTER TABLE customers OWNER TO root;

--
-- Name: customers_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE customers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE customers_id_seq OWNER TO root;

--
-- Name: orders; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE orders (
    id integer NOT NULL,
    customer_id integer,
    product_name character varying(100) NOT NULL
);


ALTER TABLE orders OWNER TO root;

--
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE orders_id_seq OWNER TO root;

--
-- Name: sac; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE sac (
    id integer NOT NULL,
    customer_id integer,
    order_id integer,
    customer_name character varying(100) NOT NULL,
    title character varying(100) NOT NULL,
    obs text NOT NULL
);


ALTER TABLE sac OWNER TO root;

--
-- Name: sac_id_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE sac_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sac_id_seq OWNER TO root;

--
-- Data for Name: customers; Type: TABLE DATA; Schema: public; Owner: root
--

COPY customers (id, name, email) FROM stdin;
1	Luis	luis@bbb.com.br
2	José	jose@bbb.com.br
3	Maria	maria@bbb.com.br
4	Carla	carla@bbb.com.br
6	Vivi	vivi@bbb.com.br
5	Renata	renata@bbb.com.br
\.


--
-- Name: customers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('customers_id_seq', 6, true);


--
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: root
--

COPY orders (id, customer_id, product_name) FROM stdin;
1	1	Teclado
2	1	Monitor
3	2	Mac
4	3	Mouse comum
5	4	HD
6	6	Notebook
\.


--
-- Name: orders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('orders_id_seq', 10, true);


--
-- Data for Name: sac; Type: TABLE DATA; Schema: public; Owner: root
--

COPY sac (id, customer_id, order_id, customer_name, title, obs) FROM stdin;
1	1	1	Luis	Chamado 1	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc feugiat egestas odio, in gravida diam.
2	3	4	Maria	Chamado 2	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc feugiat egestas odio, in gravida diam.
3	4	5	Carla	Chamado 3	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc feugiat egestas odio, in gravida diam.
4	6	6	Vivi	Chamado 4	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc feugiat egestas odio, in gravida diam.
5	2	3	José	Chamado 5	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc feugiat egestas odio, in gravida diam.
6	1	2	Luis	Chamado 6	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc feugiat egestas odio, in gravida diam.
\.


--
-- Name: sac_id_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('sac_id_seq', 6, true);


--
-- Name: customers_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY customers
    ADD CONSTRAINT customers_pkey PRIMARY KEY (id);


--
-- Name: orders_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- Name: sac_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY sac
    ADD CONSTRAINT sac_pkey PRIMARY KEY (id);


--
-- Name: idx_1ab651f8d9f6d38; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX idx_1ab651f8d9f6d38 ON sac USING btree (order_id);


--
-- Name: idx_1ab651f9395c3f3; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX idx_1ab651f9395c3f3 ON sac USING btree (customer_id);


--
-- Name: idx_e52ffdee9395c3f3; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX idx_e52ffdee9395c3f3 ON orders USING btree (customer_id);


--
-- Name: fk_1ab651f8d9f6d38; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY sac
    ADD CONSTRAINT fk_1ab651f8d9f6d38 FOREIGN KEY (order_id) REFERENCES orders(id);


--
-- Name: fk_1ab651f9395c3f3; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY sac
    ADD CONSTRAINT fk_1ab651f9395c3f3 FOREIGN KEY (customer_id) REFERENCES customers(id);


--
-- Name: fk_e52ffdee9395c3f3; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT fk_e52ffdee9395c3f3 FOREIGN KEY (customer_id) REFERENCES customers(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

